#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <Wire.h>
#include <EEPROM.h>
#include <SoftwareSerial.h>

extern "C" {
#include "user_interface.h"
}

SoftwareSerial swSer(13, 15, false, 256);

#define id  "ST#0300"//Identificador de equipo para los mensajes de texto.
#define Read_key "da208106cc7d1d20";  //KEY para poder leer los datos del Host.
#define Write_key "50ce5b31c98719ee";  //KEY para poder enviar los datos al Host.

#define DEBUG 1 //Indicador del debug activado / desactivado.
#define Firmware "Firmware v1.57"

//Constantes correspondientes al I2C
#define sdaPin 4 //Pin "data" del I2C.
#define sclPin 5 //Pin "clock" del I2C.

//#define sdaPin D1
//#define sclPin D2

//Constante correspondientes a la EEPROM
#define RegLength 21 //Largo de los registros guardados en la EEPROM.

//Constantes de conexion WiFi
#define timeout 10000

//Constante correspondientes a la Bateria
#define BatlvlMin 640 //Nivel de bateria desde el cual empezara a tirar alarma de bateria baja.

//Variables correspondientes al Serial
String inputString = "";

//Strings donde se guardaran los datos de informacion.
String received = "";
String rv = "";
String Fecha = "";
String mac = "";

//Variables correspondientes al ESP8266
int WiFiTry = 40; //Intentos de conexion del WiFi antes de abortar. (Cada intento demora 500 ms aprox.).
int jph = 0; //Contador de mensajes guardados en la EEPROM sobre la cadena recibida.
int ErrTry = 0; //Cuenta la cantidad de errores, si llega a 5 se duerme el modulo.
bool Modrec = false;
int WiFiRecv = 3;

//Variables correspondientes a la Bateria
int Batlvl = 0; //Nivel de bateria (590 = 5.1v aprox - Minimo para funcionar 5.0V en Modulo A6).

//Variables correspondientes al A6GSM
int A6ok = 0; //Paso de configuracion.
bool A6 = false; //Indicador de encendido del modulo A6.
bool ConfigA6 = false; //Indicador de configuracion de A6 en proceso.
bool SMSRead = false; //indicador de lectura de SMS en memoria completado.
bool SMSReading = false; //Indicado de lectura de SMS en proceso.
bool SMSDel = false; //Indicador de borrado de SMS en memoria.
int CMGR = 0; //Cantidad de mensajes en memoria.
int CMGRi = 0; //Indice de mensaje a leer.

//Variables correspondientes a la EEPROM
String chasis = ""; //Chasis o camara asignada al equipo.x
String ssid = "Sealswifi"; //Nombre de la red WiFi.
String password = "SealsTecno455Wifi"; //Password de la red.
String host = "api.jphlions.com"; //"api.thingspeak.com"; //Host con el que nos Connectemos al enviar los datos.
String port = "80"; //Puerto del Host al cual nos Connectemos.
String phone = "1526689528"; //Numero de telefono al cual responder.
int configuration_mode = 0; //Indicador de modo configuracion activado. 
int Time = 0; //Tiempo que permanecio dormido el ESP8266.
String Pos = ""; //Posicion Indice de la tabla de registros.
String Posf = ""; //Posicion indice de la tabla para el proximo registro.


//Variables para enviar/recibir SMS
String SMS = ""; //Mensaje a enviar por SMS.
bool SMSSending = false; //Indicador de envio de SMS en proceso.
int SMStry = 3; //Cantidad de intentos ante errores al envio de un SMS.
int SMSStep = 0; //Paso del envio de un sms.

int Error = 0;

void setup() {
  #if DEBUG
  Serial.begin(115200);
  #endif
  
  swSer.begin(115200);
  EEPROM.begin(4096);
  delay(10);

  char i = EEPROM.read(4095);

  Serial.println();

  if(i != '4'){
    Serial.println("Iniciando Borrado de EEPROM");
    for (int i = 0; i < 4096; ++i) {
      EEPROM.write(i, 0);
      delay(10);
    }
    EEPROMWrite(4095, 1, "4");
    Serial.println("EEPROM Borrada");
  }

  Wire.begin(sdaPin, sclPin);
  Wire.setClockStretchLimit(25000);

  delay(100);
  

  //Cargo los valores de la EEPROM en memoria.
  EEPROMRead();

  delay(150);

  Batlvl = analogRead(A0);
  delay(10);
 
  #if DEBUG
    Serial.print(F("Battery level: "));
    Serial.println(Batlvl);
  #endif

  mac = WiFi.macAddress();
  Serial.print(F("Mac address: "));
  Serial.println(mac);

  //Reviso si tengo los datos para conectarme a una red WiFi, e intento loguearme a la misma.
  if(ssid.length()>1){
    Connect();
  }else{
    #if DEBUG
    Serial.println(F("No se encontro configuracion para la red WiFi."));
    #endif
  }

  delay(250);

  I2CRequest();

  delay(250);

  if(Error != 0){Sleep();}

  if((configuration_mode == 0) or (configuration_mode == 1)){
    if((A6 == false) and (configuration_mode == 1)){
      Serial.println("Solicitando encendido del modulo GSM");
      I2CWrite(1); 
      A6 = true;
      delay(250);
    }
    if(WiFi.status() == WL_CONNECTED){
      JPH_Receive();
      delay(100);
      Batlvlcheck();
    }else{            
      if(A6 == false){
        if(received.length() <= 1){
          Sleep();
        }else if (received.length() > 1){
          Serial.println(F("Solicitando encendido del Modulo GSM"));
          I2CWrite(1);    
          A6 = true;
          delay(250);
        }      
      }else{A6Config();}
    }
  }else{
    if(A6 == true){I2CWrite(2); A6 = false; delay(100);}
    if(WiFi.status() == WL_CONNECTED){JPH_Receive();delay(100);Batlvlcheck();}
  }
}

void A6Config() {
    switch (A6ok){
    case 0:
      Serial.println(F("Resetting GSM"));
      A6ok++;
      I2CWrite(3);
      break;
    case 1:
      delay(20);
      swSer.println("AT");
      delay(20);
      #if DEBUG
      Serial.println(F("Starting GSM configuration"));
      Serial.println("AT");
      #endif
      break;
    case 2:
      swSer.println("AT+CSDH=1");
      delay(20);
      #if DEBUG
      Serial.println(F("AT+CSDH=1"));
      #endif
      break;
    case 3:
      swSer.println("AT+CMGF=1");
      delay(20);
      #if DEBUG
      Serial.println(F("AT+CMGF=1"));
      #endif
      break;
    case 4:
      swSer.print("AT+CPMS=");
      swSer.write(0x22);
      swSer.print("SM");
      swSer.write(0x22);
      swSer.print(",");
      swSer.write(0x22);
      swSer.print("SM");
      swSer.write(0x22);
      swSer.print(",");
      swSer.write(0x22);
      swSer.print("SM");
      swSer.write(0x22);
      swSer.write(0x0D);
      swSer.write(0x0A);
      break;
    case 5:
      swSer.println("AT+CNMA=1");
      break;
    case 6:
      if(Fecha == ""){
        swSer.println("AT+CCLK");
        }
        else{
          A6ok++;
          A6Config();
        }
      break;
    case 7:
      #if DEBUG
      Serial.println(F("GSM configuration completed"));
      Serial.print(F("Message to read: "));
      Serial.println(CMGR);
      #endif
      if(CMGR == 0){SMSRead = true;}
      ConfigA6 = false;
      Batlvlcheck();
      break; 
  }
}

void I2CRequest() {
  #if DEBUG
  Serial.println(F("Write data"));
  #endif
  Wire.beginTransmission(8);
  Wire.write(0);
  Wire.endTransmission(true);
  #if DEBUG
  Serial.println(F("Receive data"));
  #endif
  Wire.requestFrom(8, 30);
  received = "";
  while (Wire.available()) {
    char b = Wire.read();
    if((b != '\n')and(b != '\r')and(b != '\0')){
      received += b;
    }
  }
  I2CEval();
}

void I2CWrite(byte x) {
  Wire.beginTransmission(8);
  Wire.write(x);
  Wire.endTransmission();
  #if DEBUG
  Serial.println(F("Data transmited"));
  #endif
}

void I2CEval() {
  int i = received.length();
  #if DEBUG
  Serial.print(F("I2C length string: "));
  Serial.println(i);
  Serial.print(F("I2C string: "));
  Serial.println(received);
  #endif
  
  if (received[0] == 'H'){
    A6 = true;
  }else if(received[0] == 'L'){
    A6 = false;
  }else{
    Error = 11;
  }

  received.remove(0, 1);

  if(received.length() > 1){
    int i = 0;
    int j = 0;
    String t = "";
    i = received.length();
    j = i - 4;
    for (int k = j; k < i; ++k){
      t += received[k];
    }
    Time = t.toInt();
    received.remove(j);
  }
  #if DEBUG
  if(Error != 0){
    Serial.print(F("Error: "));
    Serial.println(Error);
  }else{
    Serial.print(F("Tiempo dormido: ")); Serial.println(Time);
    Serial.print(F("GSM State - 0 = Off, 1 = On: ")); Serial.println(A6);
    Serial.print(F("received string: ")); Serial.println(received);}
  #endif
  return;
}

void SerialEval(String incoming) { 
  #if DEBUG
  Serial.print("SerialEval: ");
  Serial.println(incoming);
  #endif  
  if (incoming.startsWith("+CMT")) {
    SMSAvoid;
    SMSRead = false;
    CMGR++;
  }
  else if (incoming.startsWith("RING")){
  swSer.println("ATH");
  delay(20);
  #if DEBUG
  Serial.print("Hanging all the calls"); 
  #endif
  }
  else if (incoming.startsWith("OK")) {
    ErrTry = 0;
    if (ConfigA6 == false){
     A6ok++;
     A6Config();
     }
  else if (SMSReading == true){
    if(CMGR == CMGRi){
      SMSRead = true;
      SMSReading = false;
    }     
  }
  else if (SMSSending == true) {
    switch (SMSStep){
      case 0:
      SMSStep++;
      SMSSend(SMS);
      break;
      case 1:
      SMSStep = 0;
      SMSSending = false;
      break;}
    }
  }
  else if (incoming.startsWith("+CCLK")) {
    incoming.remove(0, 8);
    for (int i = 0; i < 19; ++i) {
      if ((incoming[i] != '/') and (incoming[i] != ',') and (incoming[i] != ':')) {Fecha += incoming[i];}
    }
    A6ok++;
    #if DEBUG
    Serial.println(Fecha);
    #endif
    A6Config();
    return;
  }
  else if (incoming.startsWith("+CMGR")) {
    CMGRi++;
    delay(10);
    return;
  }
  else if (incoming.startsWith("+CPMS")) {
    int Uno = 0;
    int Dos = 0;
    
    Uno = incoming.indexOf(':');
    Dos = incoming.indexOf(',');

    String Aux = "";
    int Auxn = Dos - Uno;
    
    if(Auxn == 3){
      Aux = incoming[Uno +2];}
    else{
     Aux = incoming[Uno +2];
     Aux = incoming[Uno +3]; 
    }

    CMGR = Aux.toInt();
    #if DEBUG
    Serial.print(F("Messages in memory: "));
    Serial.println(CMGR);
    #endif
    return;
  }
  else if (incoming.indexOf("^CINIT") != -1){
      ConfigA6 = false;
      A6ok = 1;
      if(incoming.indexOf("^CINIT: 32") != -1){
        A6Config();
      }
  }
  else if (incoming.indexOf("ERROR") != -1) {
    delay(1000);
    ErrTry++;
    if (ErrTry == 10){Sleep();}
    if ((A6 == true) and (ConfigA6 == false)){
     A6Config();}
    return;
  }
  else if (incoming.indexOf("COMMAND NO RESPONSE") != -1) {
    ErrTry++;
    if ((A6 == true) and (ConfigA6 == false)){
     A6Config();
    }
    else if ((ConfigA6 == true) and (SMSSending == true)){
     SMSSend(SMS);
    }
  }
  else if (incoming.indexOf("SMSFULL") != -1){
      delay(1000);
      SMSDelete(4);
  }
  else if (incoming.indexOf("GSM: ON") != -1) {
      I2CWrite(1);
      delay(1000);
      String Msg;
      Msg += id;
      Msg += " - Salida 1: On";
      #if DEBUG
      Serial.println(Msg);
      #endif
      if(A6=true){SMSSend(Msg);}
    }
  else if (incoming.indexOf("GSM: OFF") != -1) {
      I2CWrite(2);
      delay(1000);
      String Msg;
      Msg += id;
      Msg += " - Salida 1: OFF";
      #if DEBUG
      Serial.println(Msg);
      #endif
      if(A6=true){SMSSend(Msg);}
    }
  else if (incoming.indexOf("Chasis:") != -1) {
      int Uno = 0;
      int Dos = 0;
      int Borrar = 0;
    
      Uno = incoming.indexOf(':');
      Dos = incoming.indexOf(':', Uno +1);
    
      if(Uno < Dos){
        incoming.remove(0, Dos+2);}
      else{
        incoming.remove(0, Uno+2);
      }
      WiFi.disconnect();
      EEPROMWrite(0, 10, incoming);

      chasis = incoming;

      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se ha modificado el chasis a: ";
        Msg += incoming;
        SMSSend(Msg);}
    }
  else if (incoming.indexOf("SSID:") != -1) {
      int Uno = 0;
      int Dos = 0;
      int Borrar = 0;
    
      Uno = incoming.indexOf(':');
      Dos = incoming.indexOf(':', Uno +1);
    
      if(Uno < Dos){
        incoming.remove(0, Dos+2);}
      else{
        incoming.remove(0, Uno+2);
      }

      if(WiFi.status() == WL_CONNECTED){WiFi.disconnect();}
      
      EEPROMWrite(10, 32, incoming);

      ssid = incoming;

      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se ha modificado el SSID a: ";
        Msg += incoming;
        SMSSend(Msg);}      
    }
  else if (incoming.indexOf("Pass:") != -1) {
      int Uno = 0;
      int Dos = 0;
      int Borrar = 0;
    
      Uno = incoming.indexOf(':');
      Dos = incoming.indexOf(':', Uno +1);
    
      if(Uno < Dos){
        incoming.remove(0, Dos+2);}
      else{
        incoming.remove(0, Uno+2);
      }

      if(WiFi.status() == WL_CONNECTED){WiFi.disconnect();}
      
      EEPROMWrite(42, 64, incoming);

      password = incoming;
      delay(500);
      
      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se modifico el Password a: ";
        Msg += incoming;
        SMSSend(Msg);}     
  }
  else if (incoming.indexOf("Port:") != -1) {
      int Uno = 0;    
      int Dos = 0;
      int Borrar = 0;
    
      Uno = incoming.indexOf(':');
      Dos = incoming.indexOf(':', Uno +1);
    
      if(Uno < Dos){
        incoming.remove(0, Dos+1);}
      else{
        incoming.remove(0, Uno+1);
      }
      EEPROMWrite(170, 4, incoming);

      port = incoming;
      
      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se ha modificado el puerto del Host a: ";
        Msg += incoming;
        SMSSend(Msg);}     
    }
  else if (incoming.indexOf("Host:") != -1) {
      int Uno = 0;
      int Dos = 0;
      
      Uno = incoming.indexOf(':');
      incoming.remove(0, Uno+2);
      Dos = incoming.indexOf("AT+CMGD=1,4");
      incoming.remove(Dos);

      #if DEBUG
      Serial.println(incoming.length());
      Serial.println(incoming);
      #endif
      
      EEPROMWrite(106, 64, incoming);

      host = incoming;

      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se ha modificado el Host a: ";
        Msg += incoming;
        SMSSend(Msg);}     
    }
  else if (incoming.indexOf("Tel:") != -1) {
      int Uno = 0; 
      int Dos = 0;
      int Borrar = 0;
    
      Uno = incoming.indexOf(':');
      Dos = incoming.indexOf(':', Uno +1);
    
      if(Uno < Dos){
        incoming.remove(0, Dos+2);}
      else{
        incoming.remove(0, Uno+2);
      }
      EEPROMWrite(174, 10, incoming);

      phone = incoming;
      
      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se ha modificado el numero telefonico a donde se envian los SMS al: ";
        Msg += phone;
        SMSSend(Msg);}
    }
  else if (incoming.indexOf("Configuracion: ") != -1) {
      int Uno = 0;    
      int Dos = 0;
      int Borrar = 0;
    
      Uno = incoming.indexOf(':');
      Dos = incoming.indexOf(':', Uno +1);

      if(Uno < Dos){
        incoming.remove(0, Dos+1);}
      else{
        incoming.remove(0, Uno+1);
      }
      
      EEPROMWrite(184, 1, incoming);

      String Aux = "";
      Aux += incoming[0];
      int Iaux = Aux.toInt();
      
      configuration_mode = Iaux;
      
      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Modo configuracion: ";
        Msg += Iaux;
        SMSSend(Msg);}
    }
  else if (incoming.indexOf("Borrar SMS") != -1) {
    SMSDelete(4);
    CMGR = 0;
    CMGRi = 0;
    
    String Msg = "";
    Msg += id;
    Msg += " - Se han borrado todos los SMS en memoria";
    
    #if DEBUG
    Serial.println(Msg);
    #endif
      
    if(A6=true){SMSSend(Msg);}
    }
  else if (incoming.indexOf("Borrar Tabla") != -1) {
      TableDelete();
      delay(3000);
      if(A6=true){
        String Msg = "";
        Msg += id;
        Msg += " - Se ha eliminado la informacion almacenada";
        SMSSend(Msg);}
    }
  else if (incoming.indexOf("Enviar Tabla") != -1) {
      TableSend();
    }
  else if (incoming.indexOf("Reset") != -1) {
      EEPROMReset();
    }
  else if (incoming.indexOf("Reboot") != -1){
    if ((A6 == true) and (ConfigA6 == true)){
      SMSDelete(4);
    }
    delay (2000);  
    //ESP.reset();
    system_restart;
  }
  else if (incoming.indexOf("Intensidad wifi") != -1){
    String Msg = "";
    if((phone != "") and (ssid != "")){
      if (WiFi.status() != WL_CONNECTED) { 
        Msg = "Couldn't get a wifi connection";
        SMSSend(Msg);
        #if DEBUG
        Serial.println(Msg);
        #endif
      } 
      else {
        int rssi = WiFi.RSSI();
        String Msg = "";
        Msg += "Signal strength: "; Msg += rssi; Msg += " dB";
        if(rssi >= -90){
          if(rssi >= -80){
            if(rssi >= -70){
              if(rssi >= -67){
                if(rssi >= -30){
                  Msg += " - Amazing";
                  SMSSend(Msg);
                  #if DEBUG
                  Serial.println(Msg);
                  #endif
                  return;
                }
                Msg += " - Very Good";
                SMSSend(Msg);
                #if DEBUG
                Serial.println(Msg);
                #endif
                return;
              }
              Msg += " - Good";
              SMSSend(Msg);
              #if DEBUG
              Serial.println(Msg);
              #endif
              return;
           }
           Msg += " - Poor";
           SMSSend(Msg);
           #if DEBUG
           Serial.println(Msg);
            #endif
           return;
          }
          Msg += " - Unusable";
          SMSSend(Msg);
          #if DEBUG
          Serial.println(Msg);
          #endif
          return;
        }
      }
    }
    else{
      return;
    }
  }
  else{
      return;
    }
}

void Serialread(){
  while (swSer.available()) {
      inputString = swSer.readStringUntil('\n');
      /*
      #if DEBUG
      Serial.print(F("Comenzando evaluacion de: "));
      Serial.println(inputString);
      #endif
      */
      delay(40);
      SerialEval(inputString);
      inputString = "";
  }
}

void Connect() {
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(ssid.c_str(), password.c_str());
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      #if DEBUG
      Serial.print(".");
      #endif
      if (WiFiTry == 0) {
        #if DEBUG
        Serial.println(); Serial.println(F("Connection failed"));
        #endif
        break;
      }
      WiFiTry--;
    }
  }
  if (WiFiTry != 0) {
    #if DEBUG
    Serial.println("");
    Serial.println(F("WiFi connected"));
    Serial.print(F("IP address: "));
    Serial.println(WiFi.localIP());
    #endif
  }
}

void JPH_Send(String Mensaje) {
  String Temp = ""; //Tiempo que estuvo activo en ese estado las entradas/salidas.

  #if DEBUG
  Serial.print(F("connecting to "));Serial.println(host);
  #endif
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = port.toInt();
  
  if (!client.connect(host.c_str(), httpPort)) {
    #if DEBUG
    Serial.println("connection failed");
    #endif
    return;
  }
  
  String url = "/update.php?key=";
  url += Write_key;
  url += "&datos=";
  url += Mensaje[0]; 
  url += Mensaje[1];
  url += Mensaje[2];
  url += Mensaje[3];
  url += Mensaje[4];
  url += Mensaje[5];
  url += Mensaje[6];
  
  url += Fecha;
  
  #if DEBUG 
  Serial.print("Requesting URL: ");
  Serial.println(url);
  #endif
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  delay(10);

  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    #if DEBUG
    Serial.print(line);
    #endif
  }

  jph++;
}

void JPH_SendMsg(String Mensaje) {
  #if DEBUG
  Serial.print(F("connecting to "));Serial.println(host);
  #endif

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = port.toInt();
  
  if (!client.connect(host.c_str(), httpPort)) {
    #if DEBUG
    Serial.println("connection failed");
    #endif
    return;
  }
  
  String url = "/update.php?key=";
  url += Write_key;
  url += "&info=";
  url += Mensaje;
  
  #if DEBUG 
  Serial.print("Requesting URL: ");
  Serial.println(url);
  #endif
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  delay(10);
}

void JPH_Receive() {
  #if DEBUG
  Serial.println(F("Creating TCP Connection"));
  #endif
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = port.toInt();
  int i = 3;

  #if DEBUG
  Serial.print(F("connecting to ")); Serial.println(host);
  #endif

  //client.setTimeout(1500);
  
  while (!client.connect(host.c_str(), httpPort)) {
    delay(timeout);
    client.connect(host.c_str(), httpPort);
    #if DEBUG
    Serial.println(F("connection failed"));
    #endif  
    if (i == 0) {
      break;
    }
    i--;
  }

  if (client.connect(host.c_str(), httpPort)) {
    String url = "/leer.php?key=";
    url += Read_key;

    #if DEBUG
    Serial.print(F("Requesting URL: "));Serial.println(url);
    #endif
  
    // This will send the request to the server
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n\r\n");
    /*
    delay(10);
    // Lee todas las lineas que envia como respuesta el servidor, y los analiza buscando la respuesta.
    int mi = 0;
    mi = millis();
    Serial.println(mi);
    */
    unsigned long T = millis();
    
    while(client.available() == 0){
      if(millis() - T > 5000){
        Serial.println("Client timeout");
        client.stop();
        return;
      }
    }
    while(client.available()){
      String line = client.readStringUntil('\r');
      ClientEval(line);
    }
    
    delay(3000);
    if((Fecha == "") and (WiFiRecv != 0)){
      WiFiRecv--;
      Serial.println("Conection fail, retrying...");
      JPH_Receive();
      }
    Serial.println("Closing Connection");
  }
}

void ClientEval(String incoming){
  unsigned int i = incoming.length();
     
  if(incoming.indexOf("JPH: ") != -1){
    #if DEBUG
    Serial.print(F("Mensaje a analizar")); Serial.println(incoming);
    #endif
    incoming.remove(0, 6);
    Fecha = "";
    for(int j=0; j<14; j++){
      Fecha += incoming[j];
    }

    if(incoming.length() != 14){
      int Out0 = incoming.indexOf(";"); //Estado del GSM
      int Out1 = incoming.indexOf(";", Out0 + 1); //Uso de almacenamiento en EEPROM
      int Nchas = incoming.indexOf(";", Out1 + 1); //Nombre del chasis
      int NSP = incoming.indexOf(";", Nchas + 1); //Modo de configuracion
      int NT = incoming.indexOf(";", NSP + 1); //SSID
      int PA = incoming.indexOf(";", NT + 1); //Pass
      int TEL = incoming.indexOf(";", PA + 1); //Telefono
      int HS = incoming.indexOf(";", TEL + 1); //Host
      int PT = incoming.indexOf(";", HS + 1); //Port
      int FIN = incoming.indexOf(";", PT + 1); //Fin del Mensaje

      String Aux = "";
      /*
      Out0 = incoming.indexOf(";"); //Estado del GSM
      Out1 = incoming.indexOf(";", Out0 + 1); //Uso de almacenamiento en EEPROM
      Nchas = incoming.indexOf(";", Out1 + 1); //Nombre del chasis
      NSP = incoming.indexOf(";", Nchas + 1); //Modo de configuracion
      NT = incoming.indexOf(";", NSP + 1); //SSID
      PA = incoming.indexOf(";", NT + 1); //Pass
      TEL = incoming.indexOf(";", PA + 1); //Telefono
      HS = incoming.indexOf(";", TEL + 1); //Host
      PT = incoming.indexOf(";", HS + 1); //Port
      FIN = incoming.indexOf(";", PT + 1); //Fin del Mensaje
      */
      //Evaluo la solicitud para cambiar el estado del GSM
      if(Out1 != Out0 + 1){ 
        int ini = Out0 + 1;
        String Aux = "";
        Aux += incoming[ini];
        Serial.print("Estado GSM: "); Serial.println(Aux);
        int i = Aux.toInt();  
        switch (i){
          case 0:
          Serial.println(F("Solicitando apagado de GSM"));
          I2CWrite(2);
          delay(250);
          break;
          case 1:
          Serial.println(F("Solicitando encendido de GSM"));
          I2CWrite(1);
          A6 = true;
          delay(250);
          A6Config();
          break;
          default:
          Serial.print(F("Ignorado comando: ")); Serial.println(i);
          break;
        }
      delay(250);
      }else{Serial.println("No hay cambios solicitados en el estado del GPRS");}
      
      //Manejo de informacion almacenada en la EEPROM.
      if(Nchas != Out1 + 1){ 
        int ini = Out1 + 1; 
        String Aux = "";
        String Msg = "";
        Aux += incoming[ini];
        int con = Aux.toInt();
        Serial.print("Solicitud de memoria EEPROM: "); Serial.println(Aux);
        switch(con){
          case 0:
          EEPROMReset();
          Serial.println(F("Configuracion almacenada en EEPROM eliminada"));
          break;
          case 1:
          TableSend();
          Serial.println(F("Tabla almacenada en la EEPROM enviada"));
          break;
          case 2:
          TableDelete();
          Serial.println(F("Tabla almacenada en la EEPROM borrada"));
          break;
          case 3:
          EEPROMRead();
          Msg += "&id=";
          Msg += id;
          Msg += "&mac=";
          Msg += mac;
          Msg += "&Firm=";
          Msg += Firmware;
          Msg += "&Bat=";
          Msg += Batlvl;
          Msg += "&config=";
          Msg += configuration_mode;
          Msg += "&tel=";
          Msg += phone;
          Msg += "&chasis=";
          Msg += chasis;
          delay(500);
          JPH_SendMsg(Msg);
          break;
          default:
          Serial.println("No hay cambios solicitados para la EEPROM");
          break;
        }
      }
      
      //Evaluo la solicitud de modificacion del numero de chasis
      if(NSP != Nchas + 1){ 
        int ini = Nchas + 1;
        chasis = "";
        for(int j=ini; j<NSP; j++){
          chasis += incoming[j];
        }
        EEPROMWrite(0, 10, chasis);
        delay(200);
        Serial.print(F("Se modifico el numero de chasis a: ")); Serial.println(chasis);
      }else{Serial.println("No hay cambios solicitados en el chasis");}
      
      //Evaluo la solicitud para modificar el metodo de funcionamiento (configuration_mode)
      if(NT != NSP + 1){
        int ini = NSP + 1; 
        String Aux = "";
        Aux += incoming[ini];
        int con = Aux.toInt();
        switch (con){
          case 0:
          configuration_mode = 0;
          EEPROMWrite(184, 1, "0");
          delay(20);
          Serial.println(F("El modo de configuracion fue cambiado a: 0"));
          break;
          case 1:
          configuration_mode = 1;
          EEPROMWrite(184, 1, "1");
          delay(20);
          Serial.println(F("El modo de configuracion fue cambiado a: 1"));
          break;
          case 2:
          configuration_mode = 2;
          EEPROMWrite(184, 1, "2");
          delay(20);
          Serial.println(F("El modo de configuracion fue cambiado a: 2"));
          break;
          case 3:
          configuration_mode = 3;
          EEPROMWrite(184, 1, "3");
          delay(20);
          Serial.println(F("El modo de configuracion fue cambiado a: 3"));
          break;
          default:
          Serial.println(F("Se ha ignorado la solicitud de cambio de configuracion"));
          break;
        }
      }else{Serial.println("No hay cambios solicitados en la configuracion");}

      //Modificacion del telefono
      if(HS != TEL + 1){
        phone = "";
        int ini = TEL + 1; 
        for(int j = ini; j < HS; j++){
         phone += incoming[j];
        }
        
        EEPROMWrite(174, 10, phone);
        delay(200);
        Serial.print(F("Se ha modificado el telefono correctamente a: ")); Serial.println(phone);
      }else{Serial.println("No hay cambios solicitados en el telefono");}

      JPH_SendMsg("OK");
      
      //Modificacion del SSID
      if(PA != NT + 1){
        if(WiFi.status() == WL_CONNECTED){WiFi.disconnect();}
        ssid = "";
        int ini = NT + 1; 
        for(int j = ini; j < PA; j++){
         ssid += incoming[j];
        }  
        
        EEPROMWrite(10, 32, ssid);
        delay(640);
        Serial.print(F("Se ha modificado la ssid correctamente a: ")); Serial.println(ssid);
      }else{Serial.println("No hay cambios solicitados en el ssid");}

      //Modificacion del password
      if(TEL != PA + 1){
        if(WiFi.status() == WL_CONNECTED){WiFi.disconnect();}
        password = "";
        int ini = PA + 1; 
        for(int j = ini; j < TEL; j++){
         password += incoming[j];
        }
        
        EEPROMWrite(42, 64, password);
        delay(1280);
        Serial.print(F("Se ha modificado la password correctamente a: ")); Serial.println(password);
      }else{Serial.println("No hay cambios solicitados en el password");}

      //Modificacion del Host
      if(PT != HS + 1){
        host = "";
        int ini = HS + 1; 
        for(int j = ini; j < PT; j++){
         host += incoming[j];
        }
        
        EEPROMWrite(106, 64, host);
        delay(1280);
        Serial.print(F("Se ha modificado el Host correctamente a: ")); Serial.println(host);
      }else{Serial.println("No hay cambios solicitados en el Host");}
      
      //Modificacion del Puerto     
      if(FIN != PT + 1){
        port = "";
        int ini = PT + 1; 
        for(int j = ini; j < FIN; j++){
         port += incoming[j];
        }
        
        EEPROMWrite(170, 4, port);
        delay(80);
        Serial.print(F("Se ha modificado el puerto correctamente a: ")); Serial.println(port);
      }else{Serial.println("No hay cambios solicitados en el Puerto");}     
    }
   return;
  }
}

void SMSSend(String Mensaje) {
  if(phone != ""){
    SMSSending = true;
    #if DEBUG
    Serial.println(F("Iniciando envio de SMS"));
    #endif
    swSer.print("AT+CMGF=1");
    swSer.write(0x0D);
    swSer.write(0x0A);
    delay(250);
    swSer.print("AT+CMGS=");
    swSer.write(0x22);
    swSer.print(phone.c_str());
    swSer.write(0x22);
    swSer.write(0x0D);
    swSer.write(0x0A);
    delay(500);
    swSer.print(Mensaje);
    swSer.write(char(26));
    delay(20);
    #if DEBUG
    Serial.println(F("Envio de SMS Finalizado"));
    #endif
    SMSSending = false;
  }
}

void SMSDelete(int i){
    swSer.print("AT+CMGD=1,");
    swSer.println(i);
    delay(500);
    SMSDel = true;
}

void SMSAvoid(){
  delay(100);
  CMGR++;
  swSer.print("AT+CMGR=");
  swSer.println(CMGR);
  delay(50);
}

void Batlvlcheck() {
  String Bat = "";
  Bat += Batlvl;
  if(WiFi.status() == WL_CONNECTED){JPH_SendMsg(Bat);}else{
    if (Batlvl < BatlvlMin) {
      if(A6 = true){
        SMSSend("Low Battery");
      }else{
        String Msg = "";
        Msg += id;
        Msg += " - Low Battery";
        if(Fecha != ""){
          String Lw = "";
          Lw += "LOWBATT";
          Lw += Fecha;
          RegSave(Lw);
        }
      }
    }
  }
}

void Sleep() {
  switch (configuration_mode){
    case 0:
      if ((A6 == true)and(SMSRead == true)and(SMSDel != true)){
        SMSDelete(4);
        delay(2000);
      }
      #if DEBUG
      Serial.println(F("Configurando Deep Sleep"));
      #endif
      I2CWrite(5);
      delay(250);
      #if DEBUG
      Serial.println(F("Apagando modulo GSM A6"));
      #endif
      I2CWrite(6);
      delay(250);
      #if DEBUG
      Serial.println(F("Iniciando Deep Sleep"));
      #endif
      system_deep_sleep_set_option(0);
      system_deep_sleep(0);
      yield();
    break;
    case 1:
      if ((A6 == true)and(SMSRead == true)and(SMSDel != true)){
        SMSDelete(4);
        delay(2000);
      }
      #if DEBUG
      Serial.println(F("Configurando Sleep Reset"));
      #endif
      I2CWrite(4);
      delay(250);
      I2CWrite(7);
      delay(250);
      #if DEBUG
      Serial.println(F("Iniciando Sleep Reset"));
      #endif
      system_deep_sleep_set_option(0);
      system_deep_sleep(5 * 60 * 1000000);
      yield();
    break;
    case 2:
      #if DEBUG
      Serial.println(F("Configurando Deep Sleep"));
      #endif
      I2CWrite(5);
      delay(250);
      I2CWrite(6);
      delay(250);
      #if DEBUG
      Serial.println(F("Iniciando Deep Sleep"));
      #endif
      system_deep_sleep_set_option(0);
      system_deep_sleep(0);
      yield();
    break;
    case 3:
      #if DEBUG
      Serial.println(F("Configurando Sleep Reset"));
      #endif
      I2CWrite(4);
      delay(250);
      I2CWrite(6);
      delay(250);
      #if DEBUG
      Serial.println(F("Iniciando Sleep Reset"));
      #endif
      system_deep_sleep_set_option(0);
      system_deep_sleep(0);
      yield();
    break;
  }
}

void TableDelete() {
  #if DEBUG
  Serial.println(F("Borrando tabla en EEPROM..."));
  #endif
  for (int i = 189; i < 3340; ++i) {
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
  #if DEBUG
  Serial.println(F("Tabla en EEPROM Borrada"));
  #endif

  String Position = "0189";
  EEPROMWrite(185, 4, Position);
}

void TableSend(){
  if (WiFi.status() != WL_CONNECTED) {
    for (int i = 0; i<150; i++){
      RegRead(i);
      delay(100);
      String Msg = "";
      Msg += id;
      if (received != ""){
        JPH_Send(received);
        delay(3000);
        if(A6=true){
          Msg += " - Se ha descargado la informacion almacenada.";
          SMSSend(Msg);}
      }
      else{
        if(A6=true){
          Msg += " - No es posible descargar la tabla. Equipo sin conexion Wi-Fi";
          SMSSend(Msg);}
      }
    }
    return;
  }
}

void EEPROMReset() {
  #if DEBUG
  Serial.println(F("Borrando configuracion en EEPROM..."));
  #endif
  for (int i = 0; i < 185; ++i) {
    EEPROM.write(i, 0);
    delay(10);
  }
  EEPROM.commit();
  #if DEBUG
  Serial.println(F("Configuracion en EEPROM Borrada"));
  #endif
}

void EEPROMRead(){
  String Aux = "";
  Aux = EEPROMString(0, 10);
  if(Aux != ""){chasis = Aux; Aux = "";}
  delay(100);
  #if DEBUG
  Serial.print(F("Reading EEPROM Chasis: "));Serial.println(chasis);
  #endif  
  Aux = EEPROMString(10, 42);
  if(Aux != ""){ssid = Aux; Aux = "";}
  delay(320);
    #if DEBUG
  Serial.print(F("Reading EEPROM SSID: "));Serial.println(ssid);
    #endif  
  Aux = EEPROMString(42, 106);
  if(Aux != ""){password = Aux; Aux = "";}
  delay(640);
    #if DEBUG
  Serial.print(F("Reading EEPROM Password: "));Serial.println(password);
    #endif  
  String Aux_host = "";
  Aux = EEPROMString(106, 170);
  if(Aux != ""){host = Aux; Aux = "";}
  delay(640);
    #if DEBUG
  Serial.print(F("Reading EEPROM Host: "));Serial.println(host);
    #endif  
  Aux = EEPROMString(170, 174);
  if(Aux != ""){port = Aux; Aux = "";}
  delay(80);
    #if DEBUG
  Serial.print(F("Reading EEPROM Port: "));Serial.println(port);
    #endif  
  Aux = EEPROMString(174, 184);
  if(Aux != ""){phone = Aux; Aux = "";}
  delay(200);
    #if DEBUG
  Serial.print(F("Reading EEPROM phone: "));Serial.println(phone);
    #endif  
  Aux = EEPROMString(184, 185);
  if(Aux != ""){
    int i = Aux.toInt();
    switch (i){
      case 0:
      configuration_mode = 0; 
      break;
      case 1:
      configuration_mode = 1; 
      break;
      case 2:
      configuration_mode = 2; 
      break;
      default:
      configuration_mode = 3; 
      break;
    }
  }
  Aux = "";
  #if DEBUG
  Serial.print(F("Reading Configuration Mode: "));Serial.println(configuration_mode);
  #endif  
}

String EEPROMString(int posi, int posf){
    String Aux = "";
    char b;
    for (int i = posi; i < posf; ++i){
    b = char(EEPROM.read(i));
    if (b != '\0'){
      if((b != '\n')and(b != '\r')){
        Aux += b;
        delay(10);}
    }
    else{
      break;}
    }
    return Aux;
}

void EEPROMWrite(int index, int maxlength, String text) { //Graba en la EEPROM, Se le asigna posicion inicial, largo maximo, largo cadena, cadena de texto a grabar.
  char b;
  int j = text.length();
  int k = 0;
  
  for (int i = 0; i < maxlength; ++i) {
    b = char(EEPROM.read(i));
    k = i + index;
    
    if(i >= j){
      if(b == 0){}else{EEPROM.write(k, 0);}
    }else{
      if(b == char(text[i])){}
      else{
        EEPROM.write(k, char(text[i]));
        delay(10);}
    }
  }
  
  EEPROM.commit();
  
  delay(maxlength * 10);
  #if DEBUG
  Serial.println(F("Se ha grabado la siguiente informacion en la EEPROM: "));Serial.println(text);
  #endif
}

void RegSave(String Texto){
  Pos = "";

  for (int i = 185; i < 189; ++i)  {
        Pos += char(EEPROM.read(i));
        delay(10);}

  delay(100); 

  #if DEBUG
  Serial.print(F("Reading Table index - EEPROM read: "));Serial.println(Pos);
  #endif
  
  int Posi = Pos.toInt();
  
  if ((Posi == 0)or(Posi > 3339)){Posi = 189;}
  
  Texto += Fecha;
    
  EEPROMWrite(Posi, RegLength, Texto);

  Posi = Posi + RegLength;
  if(Posi < 1000){Posf += "0";}
  Posf = "";
  Posf += String(Posi);

  EEPROMWrite(185, 4, Posf);

  #if DEBUG
  Serial.print(F("Next Table index - EEPROM save: "));Serial.println(Posi);
  #endif
}

void RegRead(int k){
  #if DEBUG
  Serial.println(F("Reading EEPROM: "));
  #endif
  
  received = "";
  Fecha = "";
 
  int j = 189 + k * RegLength;
  int l = j + RegLength;
  int m = 0;

  #if DEBUG
  Serial.print(F("Posicion inicial: "));Serial.println(j);
  Serial.print(F("Posicion final: "));Serial.println(l);
  #endif
  
  for (int i = j; i < l; ++i)
  {
    m = i-j;
    if((m) < 7){  
      received += char(EEPROM.read(i));
      delay(10);}
    else{
      Fecha += char(EEPROM.read(i));
      delay(10);}  
  }
  delay(RegLength * 10);
}

void RegEEPROM(){
  #if DEBUG
  Serial.println(F("Comenzando el guardado de informacion en la EEPROM"));
  #endif

  receivedMod();
  delay(100);
  
  String Msg = "";
  String incoming = "";
  int k = received.length();
  for (int i = 0; i < 7; ++i){
    Msg += received[i];
  }
  RegSave(Msg);
  delay(200);
  Msg = "";
  if(k == 7){received = "";}
  if(k> 7){
    for (int i = 7; i < 14; ++i){
    Msg += received[i];
  }
  RegSave(Msg);
  }
  delay(200);
  Msg = "";
  if(k == 14){received = "";}
  if(k> 14){
    for (int i = 14; i < 21; ++i){
      Msg += received[i];
    }
    RegSave(Msg);
  }
  delay(200);
  received = "";
  #if DEBUG
  Serial.println(F("Finalizado el guardado de informacion en la EEPROM"));
  #endif
  if((configuration_mode == 0) and (A6 == true) and (phone.length()>1)){ 
    String Msg = "";
    Msg += id;
    Msg += " - ¡Atención! Ocurrio una modificación en el estado del equipo.";
    SMSSend(Msg);
  }
}

void RegJPH(){
  #if DEBUG
  Serial.println(F("Comenzando envio de informacion al sitio de JPH"));
  #endif

  if(jph == 0){
  receivedMod();
  delay(100);}
  
  int k = received.length();
  String Msg = "";
  switch (jph){
    case 0:
      for (int i = 0; i < 7; ++i){
        Msg += received[i];
      }
      JPH_Send(Msg);
      if(k == 7){received = "";}
    break;
    case 1:
      if(k> 8){
        for (int i = 7; i < 14; ++i){
          Msg += received[i];
        }
        JPH_Send(Msg);
      }
      if(k == 14){received = "";}
    break;
    case 2:
      if(k> 14){
        for (int i = 14; i < 21; ++i){
          Msg += received[i];
        }
        JPH_Send(Msg);
      }
      if(k >= 21){received = "";}
    break;
  }
}

void A6Work(){
  if (ConfigA6 == true){
    if (CMGR != 0){
      Serial.println("CMGR != 0");
      SMSReading = true;
      if (CMGR > CMGRi){
        int Aux = CMGRi + 1;
        swSer.print("AT+CMGR="); swSer.println(Aux);
        delay(2000);
        #if DEBUG
        Serial.print("Leyendo mensaje: "); Serial.print(Aux); Serial.print(" de "); Serial.println(CMGR);
        #endif
        delay(750);
      }else if (CMGR == CMGRi){
        SMSRead = true;
        SMSReading = false;
        CMGR = 0;
      }
    }else{
      Serial.println("CMGR = 0");
      SMSRead = true;
      SMSReading = false;
    }
  }else{
    //if ((CMGR == 0) and (SMSSending == false) and (SMSReading == false)){Sleep();}
    if ((SMSRead == true) and (SMSSending == false) and (SMSReading == false)){if((received.length() == 0) or (configuration_mode == 1)){Sleep();}}      
  } 
}

void receivedMod(){
  int i = received.length();
  Serial.print(F("Cadena original: ")); Serial.println(received);
  if(i == 21){
    ParamMod(0, 7);
    ParamMod(7, 14);
    ParamMod(14, 21);
    received = rv;
    rv = "";
  }else if(i == 14){
    ParamMod(0, 7);
    ParamMod(7, 14);
    received = rv;
    rv = "";
  }else if(i == 7){
    ParamMod(0, 7);
    received = rv;
    rv = "";
  }
  Modrec = true;
  Serial.print(F("Cadena modificada: ")); Serial.println(received);
}

void ParamMod(int ini, int fin){
    int j = ini + 4;
    int k = j -1;
    String Aux = "";
    
    for (int i = ini; i < k; i++){
      Aux += received[i];
    }
    
    int IntAux = 0;
    String IAux = "";

    for (int i = k; i < fin; i++){
      IAux += received[i];
    }
   
    IntAux = IAux.toInt();
    delay(100);
    Serial.print(Time); Serial.print(" - "); Serial.print(IntAux);
    IntAux = Time - IntAux;
    delay(100);
    Serial.print(" = "); Serial.println(IntAux);

    if(IntAux<10){Aux += "000";}else if(IntAux<100){Aux += "00";}else if(IntAux<1000){Aux += "0";}
    
    Aux += IntAux;

    rv += Aux;
}

void loop() {
  switch(configuration_mode){
    case 0:
      if(A6 = true){A6Work();}else{if(received.length() == 0){Sleep();}}
      if((Fecha.length()>=14)and(received.length() > 0)){
        if (WiFi.status() == WL_CONNECTED){RegJPH();}else{RegEEPROM();}
      }
      if(received.length() == 0){Sleep();}
    break;
    case 1:
      if(A6 = true){A6Work();}else{Sleep();}
    break;
    case 2:
      if (WiFi.status() == WL_CONNECTED){
        if((Fecha.length()>=14)and(received.length() > 0)){
          RegJPH();
        }
      }
      Sleep();
    break;
    case 3:
      Sleep();
    break;
  }
  Serialread();
}
