---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.55------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - 

ESP8266:

 - 

Arduino:

 - Se agrega funcion de reset en caso que quede el modulo WiFi encendido por 10 min. (Reset previendo que este trabado el modulo).

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.54------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - 

ESP8266:

 - 

Arduino:

 - 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.53------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - 

ESP8266:

 - 

Arduino:

 - Se mejora el debug.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.52------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - Se agrega funcion para mandar mensajes a modo de informacion a la api.
 - Se agrega respuesta de configuracion establecida.


ESP8266:

 - Se corrige formula para que filtre 14 caracteres en vez de 15.
 - Se corrige formula de calculo de tiempo desde el evento.

Arduino:

 - 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.51------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - 

ESP8266:

 - 

Arduino:

 - Se agrega al final de AnswerSt el tiempo que permanecio dormido el ESP8266.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.50------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - 

ESP8266:

 - Se modifica ciclo loop, se utiliza un switch segun configuración.
 - Se realizan acciones especiales para cada tipo de configuración.
 - Se modifican funciones que recibe por Api:

   1- Solicitud para cambiar el estado del GPRS
	0 - Apagar GPRS
	1 - Encender GPRS
   2- Manejo de registros en memoria
	0 - Deja pasar, no realiza acciones
	1 - Solicita el envio de toda la tabla de información.
	2 - Solicita el borrado de la tabla completa.
   3- Modifica el nombre del chasis
   4- Modifica el funcionamiento del equipo modificando la variable configuration_mode. Funcionamientos segun valor:
	0.- Deep sleep, con activacion por eventos del GPRS. (Metodo de funcionamiento standard del equipo).
	1.- Sleep reset, con GPRS siempre activo. (Metodo de funcionamiento para configurar de alto consumo. Es posible configurar via GPRS o WiFi).
	2.- Deep sleep, con GPRS desactivado. (Metodo preferido para el almacenamiento y bajo consumo).
	3.- Sleep reset, con GPRS desactivado.(Metodo de funcionamiento para reconfigurar de bajo consumo. Solo es posible configurar via WiFi).
   5- Modifica el nombre del SSID.
   6- Modifica la pass para la SSID.
   7- Modifica el telefono.
 - Se eliminan las siguientes configuraciones via GPRS
	- Configuracion del tiempo de sleep
	- Configuracion: ON
	- Configuracion: OFF
 - Se agrega modificacion del configuration_mode via GPRS


Arduino:

 - Se corrige valor inicial del valor RSTWifi a true

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.49------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - 

ESP8266:

 - Se modifica ciclo loop.

Arduino:

 -

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.48------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - Se modifica completamente el funcionamiento del equipo.

ESP8266:

 - Se agrega el reseteo del A6 como primer paso de la configuracion de la misma.
 - Se agrega agrega funcion que si llega basura del I2C, hace un sleep reset.
 - Se conecta a WiFi con una ip fija.
 - Se modifica el funcionamiento del configuration_mode
 - Se agregan nuevas opciones para configuration_mode:
	0.- Deep sleep, con activacion por eventos del GPRS.
	1.- Sleep reset, con GPRS siempre activo.
	2.- Deep sleep, con GPRS desactivado.
	3.- Sleep reset, con GPRS desactivado.


Arduino:

 - El arduino maneja el Reset del ESP8266.
 - Se resetea el ESP8266 cada 60 minutos o al primer evento.
 - El contador de tiempo se vuelve a 0 cada vez que el ESP8266 solicita información.
 - Se agrega funcion para que el ESP8266 resetee el modulo GSM, lo pueda apagar o prender.
 - Se agregan funciones para cambiar los tiempos de reseteo. Con modo configuracion T = 5 seg.; sin modo configuracion T = 60 min.
 - Se agregan las siguientes funciones por I2C:
	1.- GSM: ON
	2.- GSM: OFF
	3.- GSM: Reset
	4.- WiFi Reset: 5 seg.
	5.- WiFi Reset: 3600 seg.
	6.- Sleep GSM: OFF
	7.- Sleep GSM: ON

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.47------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - Se reescribe codigo, basandose en las actualizaciones de Hardware.
 - Se modifica funcionamiento de la salida 2 y la entrada 4. Ahora el funcionamiento es el siguiente: 
   Al activar la salida dos, el optocoplador correspondiente a la entrada 4 se enciende y reinicia el ESP8266.
 - Se modifica funcionamiento de la salida 1. Ahora la misma al activarse reinicia el modulo A6.

ESP8266:

 - 

Arduino:

 - Se comparan solo las 3 entradas que quedaron activas. 
 - Se guarda registro solo de las ultimas 3 modificaciones, haciendo que la primera reinicie el modulo WiFi, y no lo vuelva a hacer por los proximos 60 seg.
 - Se almacenan hasta un tiempo de 9999 segundos.
 - El modulo GSM solo se enciende cuando lo solicita el modulo WiFi.
 - El modulo GSM se apaga cuando lo indique el modulo WiFi.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.46------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - Se agrega funcion para dormir el Arduino y reducir el consumo general del equipo.

ESP8266:

 - 

Arduino

 - Se agrega funcion de dormido durante 250 ms

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.45------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - Se descartan cambios de posteriores a v1.42, se parte de version v1.41.

ESP8266:

 - Se modifica orden de solicitud de datos. (*Pendiente)
 - Se modifica comunicacion por I2C, se cambian definiciones por nombre de puertos (Por alguna razon dejo de funcionar de la otra manera).
 - Se modifica el envio de SMS, ahora la funcion se ejecuta de una vez, no en partes. (Se busca eliminar el problema de envio de varios SMS).

Arduino

 - Se adapta logica al modulo de 4 Relays
 - Se definen estados y Pines para facilitar modificaciones.
 - Se elimina uso de 1 Relay.
 - Se modifica cadena de eventos, ahora solo son 7 datos, permitiendo el almacenamiento de hasta 4 eventos. (Pendiente)

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.44------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

ESP8266:

 - Se reemplaza comunicacion I2C por Software Serial.

Arduino

 - Se reemplaza comunicacion I2C por Software Serial.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.43------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

General: 

 - Se descartan cambios de version v1.42, se parte de version v1.41

ESP8266:

 - Se modifica orden de solicitud de datos.

Arduino

 - Se adapta logica al modulo de 4 Relays
 - Se definen estados y Pines para facilitar modificaciones.
 - Se elimina uso de 1 Relay.
 - Se modifica cadena de eventos, ahora solo son 7 datos, permitiendo el almacenamiento de hasta 4 eventos.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.42------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

Arduino:

 - Se adapta logica al modulo de 4 Relays

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.41------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

ESP8266:

 - Posibilidad de activar/desactivar DEBUG

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------Firmware v1.40------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

ESP8266:

 - Si la lectura de EEPROM no encuentra nada, tomara el valor del string definido por codigo para los siguientes valores
	- Chasis
	- SSID
	- Password
	- Host
	- Port
	- Write Key
	- Read Key
	- Phone
	- Sleep Time
	- Configuration Mode

 - Se generan funciones y disminuye tamaño de codigo

 - Se eliminan variables para optimizar uso de RAM

 - Los string del Debug ahora usan la memoria flash

 - Se agrega la funcion por SMS "Intensidad wifi", la cual medira la intensidad de señal wifi de la red a la que este conectada y respondera un SMS

---------------------------------------------------------------------------------------------------------------------------------------------------------------------