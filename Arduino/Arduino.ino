#include <Wire.h>
#include <TimerOne.h>
#include <LowPower.h>
#include <avr/wdt.h>

#define ANSWERSIZE 30
#define Firmware "Firmware v1.57"

#define GSMRelay 9
#define Out1 5
#define Out2 6

#define ON LOW
#define OFF HIGH


#define DEBUG 1

String State; //Estado de las entradas analizado a cada momento.
String StateI; //Ultimo estado de entradas registrado.

String recivemsg; //Mensaje recibido desde el ESP8266.
String AnswerSt = ""; //Respuesta almacenada para responder cuando lo solicite el ESP8266.
String Answer = ""; //Respuesta en formacion.

int Time = 0; //Control de tiempo desde el ultimo evento I2C.
bool RSTWifi = false; //Indicador si fue reiniciado el modulo WiFi.
bool RSTGSM = false; //Indicador si fue reiniciado el modulo GSM.
bool DeepSleep = false; //Indicador si esta activo el DeepSleep o el SleepReset.
bool RSTEm = false; //Indicador si se produce un reseteo de emergencia.
int RSTTime = 0; //Momento de Time en que se encendio el modulo A6.
int TimeMax = 4260; //Tiempo maximo del temporizador. Una unidad equivale a 1s, dando 71 min total.
String received = ""; //Mensaje recibido desde el ESP8266.

int t0 = 0; //Tiempo de la ultima solucitud de datos.
int t1 = 0; //Tiempo hasta el cambio de estado desde la ultima solicitud de datos.

String A6Pin = "L"; //Estado del A6GSM
int Count = 0;
  
void setup() {
  wdt_disable();
  Serial.begin(115200);
  
  Timer1.initialize(1000000); //1 s
  Timer1.attachInterrupt(ISRControl);
  
  Wire.begin(8); //Une el I2C a la direccion del bus #8
  Wire.onReceive(receiveEvent); //Registramos el evento para recibir datos
  Wire.onRequest(requestEvent); //REgistramos el evento para enviar datos

  Serial.println("Setting GPIO´s");
  pinMode(Out1, OUTPUT);//A6Reset
  pinMode(Out2, OUTPUT);//WiFiReset
  pinMode(GSMRelay, OUTPUT); //A6 on/off
  pinMode(10, INPUT); //Boton 1
  pinMode(11, INPUT); //Boton 2
  pinMode(12, INPUT); //Boton 3
  
  Serial.println("GPIO´s Init");
  digitalWrite(GSMRelay, OFF);
  digitalWrite(Out1, HIGH);
  digitalWrite(Out2, LOW);
   
  StateInit();

  wdt_enable(WDTO_2S);
}

//Funcion que cuenta el tiempo hasta 1 hora
void ISRControl() {
  if (Time >= 4260) {
    Time = 0;
  }
  else {
    Time++;
  }
}

//Funcion que se ejecuta cuando el ESP8266 requiere Datos
void requestEvent() {
  if(Time<10){AnswerSt += "000";}else if(Time<100){AnswerSt += "00";}else if(Time<1000){AnswerSt += "0";}
  AnswerSt += Time;
  delay(5);
  byte response[ANSWERSIZE];
  for (byte i=0;i<ANSWERSIZE;i++) {response[i] = (char)AnswerSt.charAt(i);}  
  delay(5);
  Wire.write(response,sizeof(response));
  Serial.print("Se respondio al ESP8266: "); Serial.println(AnswerSt);
  AnswerSt = "";
  Time = 0;
  t0 = 0;
  RSTEm = false;
  Serial.println("Request event");
  delay(500);
}

//Funcion que se ejecuta cuando el ESP8266 envia Datos
void receiveEvent(int countToRead) {
  received = "";
  while (0 < Wire.available()) {
    byte x = Wire.read();
    received += x;
  }
  #if DEBUG
  Serial.print("Receive event: "); Serial.println(received);
  #endif
  AnalizarEvent(received);
}

void AnalizarEvent(String Y){
  Serial.println(Y);
  int j = Y.toInt();
  switch(j){
    case 1:
      digitalWrite(GSMRelay, ON);
      A6Pin = "H";
      RSTTime = Time;
      delay(20);
      #if DEBUG
      Serial.println("GSM A6: ON");
      Serial.print("Estado el GSM: "); Serial.println(A6Pin);
      Serial.print("Reset Time: "); Serial.println(Time);
      #endif
      break;
    case 2:{
      if(A6Pin == "H"){
      A6Pin = "L";
      RSTGSM = false;
      digitalWrite(GSMRelay, OFF);
      delay(100);
      #if DEBUG
      Serial.println("GSM A6: OFF");
      Serial.print("Estado el GSM: "); Serial.println(A6Pin);
      #endif
      }
    }break;
    case 3:
      GSMReset(2000);
      break;
    case 4:
      Serial.println("Configurado Sleep reset: 5 seg");
      DeepSleep = false;
      delay(20);
      Serial.print("TimeMax: "); Serial.println(TimeMax);
      Serial.print("Time: "); Serial.println(Time);
      break;
    case 5:
      Serial.println("Configurado Deep Sleep: 60 min");
      DeepSleep = true;
      delay(20);
      Serial.print("TimeMax: "); Serial.println(TimeMax);
      Serial.print("Time: "); Serial.println(Time);
      break;
    case 6:
      {if(A6Pin == "H"){
      A6Pin = "L";
      RSTGSM = false;
      digitalWrite(GSMRelay, OFF);
      delay(100);
      #if DEBUG
      Serial.println("GSM A6: OFF");
      Serial.println(A6Pin);
      #endif
      }
      RSTWifi = false;
      delay(20);
      Serial.print("RSTWifi: "); Serial.println(RSTWifi);
      }break;
    case 7:
      RSTWifi = false;
      delay(20);
      Serial.print("RSTWifi: "); Serial.println(RSTWifi);
      break;
  }
}

void StateInit() {
  StateI = "";

  #if DEBUG
  Serial.println("Tomando valores iniciales");
  #endif
  
  if (digitalRead(10)== HIGH){StateI += 'H';}else{StateI += 'L';}
  delay(1);
  if (digitalRead(11)== HIGH){StateI += 'H';}else{StateI += 'L';}
  delay(1);
  if (digitalRead(12)== HIGH){StateI += 'H';}else{StateI += 'L';}
  delay(1);
}

void StateCheck() {
  State = "";
  
  if (digitalRead(10)== HIGH){State += 'H';}else{State += 'L';}
  delay(1);
  if (digitalRead(11)== HIGH){State += 'H';}else{State += 'L';}
  delay(1);
  if (digitalRead(12)== HIGH){State += 'H';}else{State += 'L';}
  delay(1);
}

void StateCompare() {
  #if DEBUG
  Serial.println("Iniciando comparacion");
  #endif
  
  StateCheck();

  if(AnswerSt.length()==0){
    if(A6Pin == "H"){AnswerSt += "H";}else{AnswerSt += "L";}
  }
  else{
    String Aux = AnswerSt;
    Aux.remove(0, 1);
    if(A6Pin == "H"){
      AnswerSt = "";
      AnswerSt += "H";
      if(Aux.length()>0){AnswerSt += Aux;}
    }
    else{
      AnswerSt = "";
      AnswerSt += "L";
      if(Aux.length()>0){AnswerSt += Aux;}
    }
  }
  


  //Funcion que realiza acciones si hubo un cambio de estado en la entrada.
  if (StateI != State){
    /*
    * Comparo los valores de los vectores StateI y State, en caso de haber diferencias modifico los valores de StateI
    * Si el tiempo t0 es menor al contador Time, el tiempo sera el contador menos Time
    */
  
    if(t0 < Time){t1 = (Time - t0) ;}else{t1 = (Time + TimeMax - t0);}
    
    Serial.print("RSTWifi: "); Serial.println(RSTWifi);
    
    if(RSTWifi == false){
      Serial.println("Solicitando reseteo de WiFi");
      ESP8266Reset(1000);
    }
    
    Answer += State;
    
    if(t1<10){Answer += "000";}else if(t1<100){Answer += "00";}else if(t1<1000){Answer += "0";}

    if(t1 <= 9999){Answer += t1;}else{Answer += "9999";}
         
    if(AnswerSt.length()>=21){ }else{AnswerSt += Answer;}

    StateI = "";
    StateI = State;
    Serial.println("Hubo una modificacion en las entradas / salidas");
  }
  Answer = "";
  t1 = 0;
}

void ESP8266Reset(int i){
  Serial.println("WiFi Module Reset");
  digitalWrite(Out2, HIGH);
  delay(i);
  digitalWrite(Out2, LOW);
  RSTWifi = true;
}

void GSMReset(int i){
  Serial.println("GSM Module Reset");
  digitalWrite(Out1, LOW); 
  delay(i);
  digitalWrite(Out1, HIGH);
  RSTGSM = true;
}

void loop() {
  Serial.println(Time);
 
  if(RSTWifi == false){
    if(DeepSleep == true){
      if(Time >= 3600){
        Serial.println("Deep Sleep: Activado");
        ESP8266Reset(1000);}
    }else{
      if(Time >= 5){
        Serial.println("Deep Sleep: Desactivado");
        ESP8266Reset(1000);}
    }
  }else{
    Serial.println("RSTWifi = true");
    if(RSTEm == false){
      if(Time >= 3600){
        if(Time >= 4200){
          Serial.println("Reseteo de emergencia");
          ESP8266Reset(1000);
          RSTEm = true;
        }
      }
      else{
        if(Time >= 600){
          Serial.println("Reseteo de emergencia");
          ESP8266Reset(1000);
          RSTEm = true;
        }
      }
    }
  }

  StateCompare(); //Comparo las entradas y salidas
  
  #if DEBUG
  Serial.print(F("AnswerSt: ")); Serial.println(AnswerSt); 
  delay(5);
  #endif

  wdt_reset();
  LowPower.powerDown(SLEEP_250MS, ADC_OFF, BOD_OFF); //Apagado del arduino por 250 ms. 
  if(Count != 3){Count++;}else{Count = 0; Time++;} //Agrego 1 segundo despues de 4 inicios.
}
